import React, { Component } from "react";
import LinkIcon from "./LinkIcon";
import LinkText from "./LinkText";
import { Row, Col, Container } from "react-bootstrap";

export default class NavigationBar extends Component {
  render() {
    const { iconLinks, textLinks } = this.props.data;
    return (
      <div className="header">
        <Container>
          <div className="header-wrapper">
            <Row>
              <Col lg={6} md={6}>
                <div className="header-left">
                  <ul>
                    {iconLinks.map((iconLink, index) => (
                      <li>
                        <LinkIcon data={iconLink} key={index} />
                      </li>
                    ))}
                  </ul>
                </div>
              </Col>
              {!this.props.isMobile ? (
                <Col lg={6} md={6}>
                  <div className="header-right text-right">
                    <ul>
                      {textLinks.map((textLink, index) => (
                        <li>
                          <LinkText data={textLink} key={index} />
                        </li>
                      ))}
                    </ul>
                  </div>
                </Col>
              ) : (
                ""
              )}
            </Row>
          </div>
        </Container>
      </div>
    );
  }
}
