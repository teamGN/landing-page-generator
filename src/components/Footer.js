import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import LinkIcon from "./LinkIcon";
import LinkText from "./LinkText";

export default class Footer extends Component {
  render() {
    const { socialBackground } = this.props;
    const { iconLinks, textLinks } = this.props.links;
    const bottomLogo = `images/${this.props.logo.src}`;
    return (
      <div className="artist">
        <Container>
          <div className="artist-wrapper">
            <Row className="align-items-center">
              <Col lg={6} md={6}>
                <div className="artist-left">
                  <div className="artist-left-logo">
                    <a href={this.props.logo.url}>
                      <img
                        src={bottomLogo}
                        alt={this.props.logo.alt}
                        title={this.props.logo.alt}
                      />
                    </a>
                  </div>
                  <div className="artist-left-link">
                    <ul>
                      {textLinks.map((textLink, index) => (
                        <li>
                          <LinkText data={textLink} key={index} />
                        </li>
                      ))}
                    </ul>
                  </div>
                  <div
                    className="artist-social d-none d-md-block"
                    style={{
                      background: socialBackground
                    }}
                  >
                    <p>Follow {this.props.name} on</p>
                    <ul>
                      {iconLinks.map((iconLink, index) => (
                        <li>
                          <LinkIcon data={iconLink} key={index} />
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
              </Col>
              <Col lg={6} md={6}>
                <div className="artist-right-form">
                  <h4>Some CTA to collect mail</h4>
                  <form>
                    <div className="artist-right-form-one">
                      <input
                        type="email"
                        name=""
                        placeholder="Your email adress"
                      />
                    </div>
                    <div className="artist-right-form-one">
                      <input
                        type="text"
                        name=""
                        placeholder="Some droplist / info collection"
                      />
                    </div>
                    <div className="artist-right-submit">
                      <button type="submit">
                        Subscribe{" "}
                        <img src="images/barr.png" alt="images not found" />
                      </button>
                    </div>
                  </form>
                </div>
              </Col>
            </Row>
          </div>
        </Container>
      </div>
    );
  }
}
