import React, { Component } from "react";

export default class LinkText extends Component {
  render() {
    const { data } = this.props;
    return (
      <a href={data.url} rel="noopener norefferer" target="_blank">
        {data.name}
      </a>
    );
  }
}
