import React, { Component } from "react";

export default class LinkIcon extends Component {
  render() {
    const { data } = this.props;
    const iconClass = `fab ${data.icon}`;
    return (
      <a href={data.url} target="_blank" rel="noopener norefferer">
        <span>
          <i className={iconClass}></i>
        </span>
      </a>
    );
  }
}
