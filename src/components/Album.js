import React, { Component } from "react";
import { Container, Row, Col, Modal } from "react-bootstrap";

function VideoModal(props) {
  return (
    <Modal
      {...props}
      dialogClassName="video-dialog"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Body>
        <iframe
          id="ytplayer"
          type="text/html"
          width="640"
          height="360"
          src={`https://www.youtube.com/embed/${props.videoId}?autoplay=1`}
          frameborder="0"
          title="asd"
        ></iframe>
      </Modal.Body>
    </Modal>
  );
}

export default class Album extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalShow: false
    };
  }

  hexToRGBArray(hex) {
    if (hex !== undefined) {
      hex = hex.replace(/#/, "");
      if (hex.length === 3) {
        hex = hex.split("");
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
      }
      const colors = [];
      colors[0] = parseInt(hex.substr(0, 2), 16);
      colors[1] = parseInt(hex.substr(2, 2), 16);
      colors[2] = parseInt(hex.substr(4, 2), 16);
      return colors;
    } else {
      return [0, 0, 0];
    }
  }

  render() {
    const { images, actionButton, title, cover, video } = this.props.data;
    const { modalShow } = this.state;
    const videoGradientFirst = this.hexToRGBArray(video.background[0]);
    const videoGradientSecond = this.hexToRGBArray(video.background[1]);
    return (
      <div className="album">
        <Container>
          <div className="album-wrapper">
            <Row className="align-items-center">
              <Col lg={7} md={7}>
                <div className="album-left">
                  <p>{title}</p>
                  <h1>
                    <img src={`images/${images[0]}`} alt="" />
                  </h1>
                  <h2>
                    <img src={`images/${images[1]}`} alt="" />
                  </h2>
                  <a href={actionButton.url} target="_blank">
                    {actionButton.title} <img src="images/arr.png" alt="" />
                  </a>
                </div>
              </Col>
              <Col lg={5} md={5}>
                <div className="album-right">
                  <img src={`images/${cover}`} alt="" />
                  {video.enabled ? (
                    <span
                      className="album-play-button"
                      style={{
                        background: video.buttonBackground
                      }}
                      onClick={() => {
                        if (this.props.isMobile) {
                          window.open(
                            `https://youtube.com/watch?v=${video.id}`,
                            "_blank"
                          );
                        } else {
                          this.setState({ modalShow: true });
                        }
                      }}
                    >
                      <img src="images/play.png" alt="Play Video" />
                    </span>
                  ) : (
                    ""
                  )}
                </div>
              </Col>
            </Row>
          </div>
        </Container>
        <VideoModal
          show={modalShow}
          onHide={() => this.setState({ modalShow: false })}
          videoId={video.id}
          style={{
            background: `linear-gradient(90deg, rgba(${videoGradientFirst[0]}, ${videoGradientFirst[1]}, ${videoGradientFirst[2]}, 0.8) 0%, rgba(${videoGradientSecond[0]}, ${videoGradientSecond[1]}, ${videoGradientSecond[2]}, 0.8) 51%)`
          }}
        ></VideoModal>
      </div>
    );
  }
}
